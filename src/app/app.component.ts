import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from './core/posts.service';
import { Post } from './interfaces/Post';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading$: Observable<boolean>;
  posts$: Observable<Post[]>;
  postForm: Partial<Post> = {};
  editing = false;
  newPostId = 1;

  constructor(private postService: PostsService) {
    this.posts$ = postService.entities$;
    this.loading$ = postService.loading$;
  }

  ngOnInit() {
    this.getPosts();
  }
  startEditing(post: Post) {
    this.editing = true;
    this.postForm = post;
  }
  onSubmit() {
    const data = Object.assign({}, this.postForm) as Post;
    if (this.editing) {
      this.update(data);
    } else {
      data.id = this.postsCount();
      this.add(data);
    }
    this.postForm = {};
    this.editing = false;
  }
  add(post: Post) {
    this.postService.add(post);
  }

  postsCount() {
    let id = 0;
    const subscription = this.posts$.subscribe(posts => (id = posts.length + 1));
    subscription.unsubscribe();
    return id;
  }

  delete(post: Post) {
    this.postService.delete(post.id);
  }

  getPosts() {
    this.postService.getAll();
  }

  update(post: Post) {
    this.postService.update(post);
  }
}
