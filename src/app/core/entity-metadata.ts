import { EntityMetadataMap, EntityDataModuleConfig, DefaultDataServiceConfig } from '@ngrx/data';
import { environment } from 'src/environments/environment';

const entityMetadata: EntityMetadataMap = {
  Post: {
    entityDispatcherOptions: {
      optimisticSaveEntities: true,
      optimisticAdd: true,
      optimisticDelete: true,
      optimisticUpdate: true
    }
  }
};

export const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: environment.rootUrl
};

export const entityConfig: EntityDataModuleConfig = {
  entityMetadata
};
