import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from '../reducers';
import { EntityDataModule, DefaultDataServiceConfig } from '@ngrx/data';
import { entityConfig, defaultDataServiceConfig } from './entity-metadata';
import { CoreEffects } from './app.effects';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  providers: [{ provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig }],
  imports: [
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([CoreEffects]),
    EntityDataModule.forRoot(entityConfig)
  ]
})
export class CoreModule {}
